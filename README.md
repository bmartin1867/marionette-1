# marionette #
###### Better Living Through Obsfucation (tm) ######
Developed by T.K. Ales @ Iowa State University

## Marionette, What Is? ##
Marionette is a python script that organizes, preprocesses, formats, configures and interprets the output of the BigBack5 Neural Network, developed by Dr. David Mackay.

## Prereqs
* [VirtualBox 5 or Greater](https://www.virtualbox.org/)
* [Oracle VirtualBox Extensions](https://www.virtualbox.org/wiki/Downloads)
* Some Flavor of Linux with Multiarch Compatability (VM is running Debian /w LXDE)
* Paitence

## Python Prereqs
* [Anaconda Python 3.6](https://www.anaconda.com/distribution/)
* Numpy
* Scipy
* pandas
* Matplotlib

## Development
Development is on an 'as-needed' basis, my to-do list is too long already, but if you ask nicely I might fix a bug. Or make it worse. 50/50.

## Legal
No statement of fitness is made or implied for this software. It might work, it might not.

## Other
Go see Blade Runner 2049.